set PATH=C:\Program Files\Python312;C:\Program Files\Python312\Scripts;C:\Program Files\CMake\bin;%PATH%
virtualenv.exe -p "C:\Program Files\Python312\python.exe" .
call .\Scripts\activate
python -m pip install setuptools
python -m pip install numpy
python -m pip install wheel
python setup.py bdist_wheel --dist-dir dist3
